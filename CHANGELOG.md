# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
[markdownlint](https://dlaa.me/markdownlint/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-01-28

### Added to 1.2.0

- Added container image check and if it does not exist, it will build the image

## [1.1.0] - 2022-01-28

### Added to 1.1.0

- Updated S3 bucket to have 
  - versioning enabled
  - encryption at rest
- Correct task tfsec syntax to execute SAST process

## [1.0.0] - 2022-01-27

### Added to 1.0.0

- Initial Release
