provider "aws" {
  region = var.region
}

########################################################################
# Create Slack Notification
########################################################################

module "notify-slack" {
  source               = "terraform-aws-modules/notify-slack/aws"
  version              = "4.24.0"
  lambda_function_name = format("slack_%s", var.s3_bucket_name)
  sns_topic_name       = var.s3_bucket_name

  slack_webhook_url = var.slack_webhook_url
  slack_channel     = var.s3_bucket_name
  slack_username    = "aws_notifier"
}

########################################################################
# Fetch SNS Topic Data
########################################################################

data "aws_sns_topic" "tp" {
  depends_on = [
    module.notify-slack,
  ]
  name = var.s3_bucket_name
}

########################################################################
# Create S3 Bucket
########################################################################

resource "aws_s3_bucket" "s3_bucket" {
  depends_on = [
    data.aws_sns_topic.tp,
  ]
  bucket        = var.s3_bucket_name
  force_destroy = true
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  versioning {
    enabled = true
  }
}

########################################################################
# Enable S3 Bucket Notification to Use Event Bridge
########################################################################

resource "null_resource" "s3_bucket_enable_event_bridge" {
  depends_on = [
    aws_s3_bucket.s3_bucket,
  ]
  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = <<EOF
    aws s3api put-bucket-notification-configuration --bucket "${aws_s3_bucket.s3_bucket.id}" --notification-configuration '{ "EventBridgeConfiguration": {} }'
  EOF
  }
}

########################################################################
# Create Event Bridge Rule
########################################################################

resource "aws_cloudwatch_event_rule" "s3_to_sns" {
  depends_on = [
    aws_s3_bucket.s3_bucket,
  ]
  name          = aws_s3_bucket.s3_bucket.id
  event_pattern = <<EOF
{
  "source": ["aws.s3"],
  "detail-type": ["Object Created"],
  "detail": {
    "bucket": {
      "name": ["${aws_s3_bucket.s3_bucket.id}"]
    }
  }
}
  EOF
}

########################################################################
# Create Event Bridge Target
########################################################################

resource "aws_cloudwatch_event_target" "s3_to_sns" {
  depends_on = [
    aws_cloudwatch_event_rule.s3_to_sns,
  ]
  arn  = data.aws_sns_topic.tp.arn
  rule = aws_cloudwatch_event_rule.s3_to_sns.name
}

########################################################################
# Create Event Bridge Policy
########################################################################

data "aws_iam_policy_document" "s3_to_sns_topic_policy" {
  depends_on = [
    aws_cloudwatch_event_target.s3_to_sns,
  ]
  statement {
    effect  = "Allow"
    actions = ["SNS:Publish"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }

    resources = [data.aws_sns_topic.tp.arn]
  }
}

resource "aws_sns_topic_policy" "s3_to_sns_policy" {
  depends_on = [
    data.aws_iam_policy_document.s3_to_sns_topic_policy,
  ]
  arn    = data.aws_sns_topic.tp.arn
  policy = data.aws_iam_policy_document.s3_to_sns_topic_policy.json
}
