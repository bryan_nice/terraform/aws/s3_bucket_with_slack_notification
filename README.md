# Terraform AWS S3 Bucket With Slack Notification

This Terraform module provisions all the necessary services in AWS to create an S3 bucket where push notifications on object create are sent to slack. It uses a combination of SNS, EventBride, and Lambda function to capture and send the message to slack. The SNS to Slack is using a 3rd party module. Below is a diagram illustrate the services and how the create message moves from the S3 bucket to a Slack channel.

![diagram](./assets/terraform_aws_s3_bucket_with_slack_notification.png)

## Assumptions

+ Executing these commands on BSD or Linux environment with Docker desktop installed
+ Taskfile is installed on system too

## Provisioning/ Deprovisioning Instructions

>___***Note***___:
>
> Before running these steps sure to execute `task build-image` to create the runtime environment.

These are required to provide the necessary values needed by Terraform variables and the provisioning process will not run.

1. Get the url and token representing [Slack Incoming WebHooks](https://api.slack.com/messaging/webhooks)
2. Slack channel created with the same name as the s3 bucket that will be created
3. Set the required environment variables

| Variable                 | Required | Default   | Description                                                                                                                                                                                |
|:-------------------------|:---------|:----------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID        | Y        |           | This is the AWS access key. It must be provided, but it can also be sourced from the AWS_ACCESS_KEY_ID environment variable, or via a shared credentials file if profile is specified.     |
| AWS_SECRET_ACCESS_KEY    | Y        |           | This is the AWS secret key. It must be provided, but it can also be sourced from the AWS_SECRET_ACCESS_KEY environment variable, or via a shared credentials file if profile is specified. |
| TF_VAR_region            | N        | us-east-1 | Default AWS region to provision all services.                                                                                                                                              |
| TF_VAR_s3_bucket_name    | Y        |           | Name of the s3 bucket to be created.                                                                                                                                                       |
| TF_VAR_slack_webhook_url | Y        |           | Slack Incoming Webhook URL and token.                                                                                                                                                      |

4. Execute the following command to execute the provisioning process

    ```bash
    task provisioner
    ```
    
    >___***NOTE***___:
    >
    > Sometimes Terraform will timeout when pulling the module `notify-slack` from Terraform Registry. Rerun the task again and it should execute without issue.

5. Push files to S3 Bucket and watch for the slack message
6. To destory these services execute the deprovisiong process with this command

    ```bash
    task deprovisioner
   ```

## Executing Test Harness

Execute the following task to invoke the test harness. Currently, it runs a static code analysis. In future releases, additional tests will be added.

```bash
task tester
```

## Taskfile Targets

| Task          | Description                                                                                   |
|:--------------|:----------------------------------------------------------------------------------------------|
| apply         | Terraform applying the plan.                                                                  |
| build-image   | Builds the runtime environment required to execute this Terraform module.                     |
| clean         | Cleans up the local workspace of Terraform artifacts.                                         |
| cli           | Spawn runtime environment containing necessary binaries for developing this Terraform module. |
| deprovision   | Terraform process to deprovision services.                                                    |
| deprovisioner | Container instance running the Terraform deprovisioning process.                              |
| destroy       | Terraform destroying resources.                                                               |
| evn-check     | Checks for required environment variables were set.                                           |
| fmt           | Terraform formatting HCL files.                                                               |
| image-check   | Check if required Docker image exits.                                                         |
| init          | Terraform initializing downloading required modules and providers.                            |
| plan          | Terraform generating a plan based the HCL files.                                              |
| provision     | Terraform process to provision services.                                                      |
| provisioner   | Container instance running the Terraform provisioning process.                                |
| tester        | Container instance running the test cases process.                                            |
| tests         | Execute test cases process.                                                                   |
| tfsec         | Security scanner on the HCL files.                                                            |

## References

+ [Enabling Amazon EventBridge](https://docs.aws.amazon.com/AmazonS3/latest/userguide/enable-event-notifications-eventbridge.html)
+ [put-bucket-notification-configuration](https://docs.aws.amazon.com/cli/latest/reference/s3api/put-bucket-notification-configuration.html)
+ [notify-slack](https://registry.terraform.io/modules/terraform-aws-modules/notify-slack/aws/latest?tab=inputs)
+ [aws_cloudwatch_event_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target)
+ [aws_cloudwatch_event_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule)
+ [aws_sns_topic_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_policy)
+ [aws_s3_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)
+ [aws_iam_policy_document](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document)
+ [Sending Messages Using Incoming Webhooks](https://api.slack.com/messaging/webhooks)
+ [tfsec](https://github.com/aquasecurity/tfsec)