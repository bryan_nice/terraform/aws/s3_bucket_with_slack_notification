variable "region" {
  type        = string
  default     = "us-east-1"
  description = "This is the AWS region. It must be provided, but it can also be sourced from the AWS_DEFAULT_REGION environment variables, or via a shared credentials file if profile is specified."
}

variable "s3_bucket_name" {
  type        = string
  default     = ""
  description = "The name of the bucket. If omitted, Terraform will assign a random, unique name."
}

variable "slack_webhook_url" {
  type        = string
  default     = ""
  description = "The URL of Slack webhook"
  sensitive   = true
}